@extends('bpanel/layouts.bpanel-app')

@section('title', 'Menús')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('page::page.page_list') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('public-menu-datatable')
        </div>
    </div>

@endsection
