<td>
    {{ $row->name }}
</td>
<td class="text-center">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-blog-'.$row->id))
</td>
<td class="text-center">
    @livewire('language::datatable-languages', ['model' => $row, 'createRouteName' => 'menu.create', 'editRouteName' => 'menu.edit'], key('languages-menu-'.$row->id))
</td>
<td class="text-center">
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'menu', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el menú?'], key('blog-buttons-'.$row->id))
</td>
