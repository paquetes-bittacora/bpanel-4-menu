
<div>
    <div class="nav-wrap">
        <div class="btn-menu">
            <span></span>
        </div><!-- //mobile menu button -->
        <nav id="menu-{{ $slug }}" class="public-menu public-menu-{{ $slug }}">
            @if($publicMenu)
                <ul class="menu">
                    @foreach($publicMenu as $menu)
                        <li class="">
                            @if($menu['show'])
                                @if (Route::has($menu['link']))
                                    <a href="{{ route($menu['link']) }}" title="">{{ $menu['label'] }}</a>
                                @else
                                    <a href="{{ $menu['link'] }}" title="">{{ $menu['label'] }}</a>
                                @endif
                            @endif
                            @if( $menu['child'] )
                                <ul class="sub-menu">
                                    @foreach( $menu['child'] as $child )
                                        @if($child['show'])
                                        <li class="">
                                            @if (Route::has($child['link']))
                                                <a href="{{ route($child['link']) }}" title="">{{ $child['label'] }}</a>
                                            @else
                                                <a href="{{ $child['link'] }}" title="">{{ $child['label'] }}</a>
                                            @endif
                                        </li>
                                        @endif
                                    @endforeach
                                </ul><!-- /.sub-menu -->
                            @endif
                        </li>
                    @endforeach
                    @endif

                </ul><!-- /.menu -->
        </nav><!-- /#mainnav -->
    </div><!-- /.nav-wrap -->
</div>
