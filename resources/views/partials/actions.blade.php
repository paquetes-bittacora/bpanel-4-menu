<div class="action-buttons">
    <a class="text-success mx-2" href="{{route($scope.'.edit', $id)}}">
        <i class="fa fa-pencil"></i>
    </a>
    <form method="POST" class="d-inline mx-2"
          action="{{route($scope.'.destroy', $id)}}">
        {!!method_field('DELETE')!!}
        @csrf
        <button id="delete_button{{$id}}" type="submit" class="btn btn-primary-outline text-danger"
                @if($disabled) disabled @endif
                onclick="return confirm('¿Está seguro de querer borrar {{$message}}')">
            <i class="fa fa-trash"></i>
        </button>
    </form>
</div>
