@extends('bpanel/layouts.bpanel-app')

@section('title', 'Crear nuevo menú')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('menu::menu.add') }}</span>
            </h4>
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('menu.store')}}">
            @csrf

            @livewire('form.input-text', ['name' => 'name', 'labelText' => __('menu::menu.name'), 'required' => true])
            @livewire('form.input-checkbox', ['name' => 'active', 'value' => 1, 'labelText' => __('menu::menu.active'), 'bpanelForm' => true, 'checked' => 'checked'])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form.save-button',['theme'=>'save'])
                @livewire('form.save-button',['theme'=>'reset'])
            </div>
            @livewire('form.input-hidden', ['name' => 'locale', 'value'=> $language])
        </form>
    </div>
@endsection
