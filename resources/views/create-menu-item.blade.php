@extends('bpanel/layouts.bpanel-app')

@section('title', 'Crear elemento del menú')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('menu::menu.add') }}</span>
            </h4>
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('page.store')}}">
            @csrf

            @livewire('form.input-text', ['name' => 'text', 'labelText' => __('menu::menu.text'), 'required' => true])
            @livewire('form.select', [ 'name' => 'type', 'required' => true, 'labelText' => 'Tipo de enlace', 'labelWidth' => 3, 'fieldWidth' => 7, 'allValues' => [
                'route' => 'Ruta del gestor',
                'url' => 'URL'
            ]])

            @livewire('form.input-text', ['name' => 'url', 'labelText' => __('menu::menu.url')])
            @livewire('form.input-text', ['name' => 'route', 'labelText' => __('menu::menu.route-name')])

            @livewire('form.input-text', ['name' => 'icon', 'labelText' => __('menu::menu.icon'), 'required' => true])

            @livewire('form.input-checkbox', ['name' => 'new_tab', 'value' => 0, 'labelText' => __('menu::menu.open-in-new-tab'), 'bpanelForm' => true])
            @livewire('form.input-checkbox', ['name' => 'active', 'value' => 1, 'labelText' => __('menu::menu.active'), 'bpanelForm' => true, 'checked' => 'checked'])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form.save-button',['theme'=>'save'])
                @livewire('form.save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script>

        /**
         * Muestra u oculta los campos para el nombre de la ruta o la URL según el tipo de enlace
         * @param value
         */
        function changeFieldsVisibility(value) {
            let routeField = $('#route');
            let urlField = $('#url');

            if (value === 'route') {
                routeField.closest('.form-group').show(0);
                routeField.attr('required', 'required');
                urlField.closest('.form-group').hide(0);
                urlField.removeAttr('required');
            } else {
                routeField.closest('.form-group').hide(0);
                routeField.removeAttr('required');
                urlField.closest('.form-group').show(0);
                urlField.attr('required', 'required');
            }
        }

        $(document).ready(function () {
            let $type = $('#type');
            changeFieldsVisibility($type.val());
            $type.change(function () {
                changeFieldsVisibility($(this).val());
            });

            let routeLabel = $('#route').closest('.form-group').find('label').prepend('<span class="text-danger">*</span>');
            let urlLabel = $('#url').closest('.form-group').find('label').prepend('<span class="text-danger">*</span>');
        });
    </script>
@endpush
