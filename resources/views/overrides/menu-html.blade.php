@php
    $currentUrl = url()->current();
@endphp

<link href="{{ Vite::asset('vendor/bittacora/laravel-menu/public/style.css')}}" rel="stylesheet">

<style>
    .dd-handle {
        cursor: move;
        color: #333;
        text-decoration: none;
        background: transparent;
        padding: 13px;
        margin-left: -0.9rem;
    }

    .dd-handle:hover {
        color: #2a80c8;
        background: transparent;
    }
</style>

<div id="nguyen-huy" class="card mt-2 mb-2">
    <div class="card-header">
        <strong>Editar menú</strong>
    </div>

    <div class="card-body">
        <input type="hidden" id="idmenu" value="{{$indmenu->id ?? null}}"/>
        <input type="hidden" id="menu-slug" value="{{$indmenu->slug}}"/>
        <div class="row">
            <div class="col-md-4">
                @include('bpanel4-public-menu::overrides.partials.left')
            </div>
            {{-- /col-md-4 --}}
            <div class="col-md-8">
                @include('bpanel4-public-menu::overrides.partials.right')
            </div>
        </div>
    </div>

    <div class="ajax-loader" id="ajax_loader">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
</div>
