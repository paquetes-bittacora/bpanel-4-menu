<div class="card mt-2">
    <input name="menu-name" id="menu-name" type="hidden" value="@if(isset($indmenu)){{$indmenu->name}}@endif">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                @if(request()->get('menu') != 0 && isset($menus) && count($menus) > 0)
                    <div class="jumbotron jumbotron-fluid p-2">
                        <div class="container">
                            <h3>Estructura del menú</h3>
                            <p class="lead">
                                Coloque cada elemento en el orden deseado. Haga click en <i
                                        class="fa fa-pencil-square-o" aria-hidden="true"></i> a la derecha del elemento para mostrar más opciones.</p>
                        </div>
                    </div>
                @elseif(request()->get('menu') == 0)
                @else
                    <div class="jumbotron jumbotron-fluid p-2">
                        <div class="container">
                            <h3>Crear elemento del menú</h3>
                            <p class="lead"></p>
                        </div>
                    </div>
                @endif

                <div id="accordion" class="">
                    @if(isset($menus) && count($menus) > 0)
                        <div class="dd nestable-menu" id="nestable">
                            <ol class="dd-list">
                                @foreach($menus as $key => $m)
                                    @include('bpanel4-public-menu::overrides.partials.loop-item', ['key' => $key])
                                @endforeach
                            </ol>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if(request()->get('menu') != 0)
        <div class="card-footer">
            <button type="button" class="btn btn-danger btn-sm submitdelete deletion menu-delete"
                    onclick="deleteMenu()" href="javascript:void(9)">Delete Menu
            </button>
            @if(isset($menus) && count($menus) > 0)
                <button type="button" class="btn btn-info btn-sm"
                        onclick="updateItem()" href="javascript:void(9)">Update All Item
                </button>
            @endif
        </div>
    @endif
</div>
