<?php

declare(strict_types=1);

return [
    'menu' => 'Menú público',
    'index' => 'Listar',
    'create' => 'Nuevo menú',
    'edit' => 'Editar menú',
];
