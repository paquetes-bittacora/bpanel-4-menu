<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Tests\Integration;

use Harimayco\Menu\WMenu;
use ReflectionClass;
use ReflectionException;
use Tests\TestCase;

final class PublicMenuOverridesTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testElModuloReemplazaLaClaseDeWMenuDeVendorConLaSuyaPropia(): void
    {
        $menu = $this->app->make(WMenu::class);

        $reflection = new ReflectionClass($menu);
        $fileName = $reflection->getFileName();

        self::assertStringContainsString('Overrides', $fileName);
    }
}
