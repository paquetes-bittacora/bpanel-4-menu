
# Instalación

Hay que hacer

```php artisan vendor:publish --provider="Harimayco\Menu\MenuServiceProvider"  ```

antes de ejecutar las migraciones.

También hay que hacer vendor publish para publicar los archivos JS.

# Uso

Una vez definido el menú en el bPanel, para mostrarlo en un blade habrá que usar este componente de Livewire:

``` @livewire('public-menu', ['slug' => 'slug-del-menú']) ```

# Notas
En el composer.json de este paquete se modifican algunas clases de harimayco/wmenu-builder    
para modificar su comportamiento sin tener que rehacer todo el editor de menús, al igual que algunas vistas. Si al actualizarse el vendor de harimayco falla este paquete, forzar la versión en el composer.json:

```"harimayco/laravel-menu": "1.4", ```  
