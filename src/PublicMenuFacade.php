<?php

namespace Bittacora\PublicMenu;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\PublicMenu\PublicMenu
 */
class PublicMenuFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'public-menu';
    }
}
