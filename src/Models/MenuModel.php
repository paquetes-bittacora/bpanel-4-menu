<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Models;

use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;

/**
 * Modelo de Eloquent para representar un menú. Este modelo sólo tendrá información básica (id, slug e hijos de tipo
 * MenuItem).
 * @throws InvalidArgumentException
 * @package Bittacora\PublicMenu\Models
 * @property string $name
 * @property string $slug
 * @property bool $active
 */
class MenuModel extends Model
{
    use HasTranslations;
    use SortableTrait;
    use Userstamps;

    public $dateFormat = 'Y-m-d H:i:s';
    public $table = 'public_menus';

    public array $sortable = [
        'sort_when_creating' => true,
        'order_column_name' => 'order_column',
    ];

    public array $translatable = [];

    public $fillable = [
        'name',
        'slug',
        'active',
    ];

    public function getTranslations(string $key = null, array $allowedLocales = null): array
    {
        $output = [];
        $translations = self::where('slug', 'like', $this->id . '_%')->get()->pluck('slug')->toArray();

        $locales = array_map(fn ($slug) => str_replace($this->id . '_', '', $slug), $translations);

        foreach ($locales as $locale) {
            $output[$locale] = [$locale => $locale];
        }

        return $output;
    }
}
