<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Elemento de menú definido a partir de una ruta de Laravel.
 * @package Bittacora\PublicMenu\Models
 */
class MenuItemModel extends Model
{
    public $dateFormat = 'Y-m-d H:i:s';

    public $table = 'public_menu_items';

    public $fillable = [
        'new_tab',
        'active',
        'target',
        'menu',
        'only_unregistered',
        'label',
        'link',
        'depth',
        'sort',
        'parent',
    ];
}
