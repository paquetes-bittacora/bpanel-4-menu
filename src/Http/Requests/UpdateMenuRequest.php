<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Http\Requests;

use CodeZero\UniqueTranslation\UniqueTranslationRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Petición para guardar un nuevo menú.
 * @package Bittacora\PublicMenu\Http\Requests
 */
class UpdateMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
                UniqueTranslationRule::for('public_menus')->ignore($this->request->get('id')),
            ],
        ];
    }
}
