<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Http\Livewire;

use Auth;
use Bittacora\PublicMenu\Models\MenuModel;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use Livewire\Component;
use Spatie\Permission\Models\Role;

/**
 * Componente de Livewire para mostrar el menú en la parte pública.
 * @package Bittacora\PublicMenu\Http\Livewire
 */
class PublicMenu extends Component
{
    public string $slug;

    public function render(): View
    {
        // Obtener ID de nuestro menú a partir del slug
        $currentLocale = App::currentLocale();
        $slug = $this->slug;
        $menuModel = MenuModel::where('slug->' . $currentLocale, $slug)->first();

        if (empty($menuModel)) {
            $publicMenu = [];
            return view('menu::livewire.public-menu', compact('publicMenu', 'slug'));
        }

        $menuName = $menuModel->id . '_' . $currentLocale;

        $publicMenu = Menu::get($menuName);

        $publicMenu = $this->addVisibility($publicMenu);

        // Mostrar la vista
        return view('menu::livewire.public-menu', compact('publicMenu', 'slug'));
    }

    /**
     * Para cada elemento del menú comprueba si debe mostrarse o no.
     * @param array $menuItems
     * @return array
     */
    private function addVisibility(array $menuItems): array
    {
        foreach ($menuItems as &$menuItem) {
            $show = false;

            if ($this->checkVisibility($menuItem)) {
                $show = true;
            }
            $menuItem['show'] = $show;

            if (!empty($menuItem['child'])) {
                $menuItem['child'] = $this->addVisibility($menuItem['child']);
            }
        }
        return $menuItems;
    }

    private function checkVisibility(array $menuItem): bool
    {
        $user = Auth::user();
        /** @var string $menuItemRole */
        $menuItemRole = !empty($menuItem['role_id']) ?
                            Role::where('id', $menuItem['role_id'])->firstOrFail()->name : /** @phpstan-ignore-line  */
                            '';

        return $this->showToAnyOne($menuItem) or
            $this->showOnlyToUnregisteredUsers($menuItem) or
            $this->showToUsersWithRole($user, $menuItemRole);
    }

    /**
     * @param array $menuItem
     * @return bool
     */
    private function showToAnyOne(array $menuItem): bool
    {
        return (false == $menuItem['only_unregistered'] and empty($menuItem['role_id']));
    }

    /**
     * @param array $menuItem
     * @return bool
     */
    private function showOnlyToUnregisteredUsers(array $menuItem): bool
    {
        return (!Auth::check() and true == $menuItem['only_unregistered']);
    }

    /**
     * @param Authenticatable|null $user
     * @param string $menuItemRole
     * @return bool
     */
    private function showToUsersWithRole(?Authenticatable $user, string $menuItemRole): bool
    {
        return (null !== $user and $user->hasRole($menuItemRole));
    }
}
