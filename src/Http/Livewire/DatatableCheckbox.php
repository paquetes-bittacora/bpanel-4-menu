<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Http\Livewire;

use Bittacora\PublicMenu\Models\MenuModel;
use Livewire\Component;

/**
 * Class DatatableCheckbox
 * @package Bittacora\PublicMenu\Http\Livewire
 */
class DatatableCheckbox extends Component
{
    public $name = '';
    public $idField = '';
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText = '';
    public $menu;

    public function mount(MenuModel $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('page::livewire.datatable-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(MenuModel $MenuModel)
    {
        $MenuModel->update(['active' => !$MenuModel->active]);
        $this->checked = $MenuModel->active;
    }
}
