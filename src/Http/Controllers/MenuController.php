<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Language\LanguageFacade;
use Bittacora\PublicMenu\DataTables\MenuDatatable;
use Bittacora\PublicMenu\Http\Requests\StoreMenuRequest;
use Bittacora\PublicMenu\Http\Requests\UpdateMenuRequest;
use Bittacora\PublicMenu\Models\MenuModel;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Connection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Controlador para el gestor de menús.
 * @package Bittacora\PublicMenu\Controllers
 */
class MenuController extends Controller
{
    public function __construct(private readonly Connection $db)
    {
    }

    /**
     * @param MenuDatatable $dataTable
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('menu.index');
        return view('bpanel4-public-menu::index');
    }

    /**
     * @param null $language
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create($language = null)
    {
        $this->authorize('menu.create');

        if (empty($language)) {
            $defaultLanguage = LanguageFacade::getDefault();
            $language = $defaultLanguage->locale;
        }

        return view('bpanel4-public-menu::create-menu', ['language' => $language]);
    }

    /**
     * @param StoreMenuRequest $request
     * @return RedirectResponse
     */
    public function store(StoreMenuRequest $request): RedirectResponse
    {
        $this->db->beginTransaction();
        try {
            $this->authorize('menu.store');
            $menu = new MenuModel();
            $menu->setLocale($request->input('locale'));
            $menu->name = $request->input('name');
            $menu->slug = '';
            $menu->active = $request->input('active');
            $menu->setHighestOrderNumber();
            $menu->save();
            $menu->slug = $menu->refresh()->id . '_' . $request->input('locale');
            $menu->save();

            // Creo los menús del plugin harimayco/laravel-menu con los idiomas para hacerlo compatible con nuestro paquete

            $languages = array_column(LanguageFacade::getActives()->toArray(), 'locale');

            foreach ($languages as $language) {
                $languageMenu = MenuModel::where('slug', $menu->id . '_' . $language)->first();

                if (null === $languageMenu) {
                    $languageMenu = new MenuModel();
                    $languageMenu->name = $request->input('name') . ' (' . $language . ')';
                    $languageMenu->slug = $menu->id . '_' . $language;
                    $languageMenu->active = $request->get('active') ?? false;
                    $languageMenu->save();
                }
            }

            $this->db->commit();
            return redirect()->route('menu.index')->with(['alert-success' => __('bpanel4-public-menu::menu.created')]);
        } catch (Exception $exception) {
            $this->db->rollBack();
            return redirect()->route('menu.index')->with(['alert-danger' => __('bpanel4-public-menu::menu.not-created')]);
        }
    }

    /**
     * @param MenuModel $model
     * @param ?string $language
     * @return View
     */
    public function editMenu(MenuModel $model, $language = null): View
    {
        $this->authorize('menu.edit');
        if (empty($language)) {
            $language = LanguageFacade::getDefault()->locale;
        }

        // Por si se activan idiomas que no estaban activados al crear un menú en el idioma por defecto, creo el menú en
        // la tabla del plugin harimayco/laravel-menu si no existe, para que no falle.
        $languageMenu = MenuModel::where('slug', $model->id . '_' . $language)->first();

        if (null === $languageMenu) {
            $languageMenu = new MenuModel();
            $languageMenu->name = $model->name . ' (' . $language . ')';
            $languageMenu->slug = $model->id . '_' . $language;
            $languageMenu->active = $model->active ?? false;
            $languageMenu->save();
        }

        $model = MenuModel::whereSlug($model->id . '_' . $language)->first();

        $model->setLocale($language);
        return view('bpanel4-public-menu::edit-menu', ['language' => $language, 'menu' => $model]);
    }

    /**
     * @param UpdateMenuRequest $request
     * @param MenuModel $model
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateMenuRequest $request, MenuModel $model)
    {
        $this->authorize('menu.update');

        $model->setLocale($request->input('locale'));
        $model->fill($request->all());
        $model->save();

        return redirect()->route('menu.index')->with(['alert-success' => __('bpanel4-public-menu::menu.updated')]);
    }

    /**
     * @param MenuModel $model
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(MenuModel $model)
    {
        $this->authorize('menu.destroy');

        if ($model->delete()) {
            // Reordeno después de borrar
            $ids = MenuModel::ordered()->pluck('id');
            MenuModel::setNewOrder($ids);

            return redirect()->route('menu.index')->with(['alert-success' => __('bpanel4-public-menu::menu.deleted')]);
        }

        return redirect()->route('menu.index')->with(['alert-danger' => __('bpanel4-public-menu::menu.not-deleted')]);
    }


    /**
     * Formulario para crear un elemento del menú
     * @return Application|Factory|View
     */
    public function createMenuItem()
    {
        $this->authorize('menu.create-menu-item');
        return view('bpanel4-public-menu::create-menu-item');
    }

    /**
     * @param Request $request
     */
    public function reorder(Request $request): void
    {
        $this->authorize('menu.reorder');
        $collect = collect($request->json()->all());
        MenuModel::setNewOrder($collect->pluck('id'), $collect->first()['position']);
    }
}
