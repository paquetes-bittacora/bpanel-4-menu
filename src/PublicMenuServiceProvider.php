<?php

/** @noinspection UsingInclusionReturnValueInspection */

declare(strict_types=1);

namespace Bittacora\PublicMenu;

use Bittacora\PublicMenu\Commands\InstallCommand;
use Bittacora\PublicMenu\DataTables\MenuDatatable;
use Bittacora\PublicMenu\Http\Livewire\DatatableCheckbox;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Termwind\Components\Li;

class PublicMenuServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('public-menu')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_public-menu_table')
            ->hasCommand(InstallCommand::class);
    }

    public function boot(): void
    {
        $this->app->register(SeedServiceProvider::class);
        $this->loadRoutesFrom(__DIR__ .'/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bpanel4-public-menu');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'menu');
        Livewire::component('menu-datatable-checkbox', DatatableCheckbox::class);
        Livewire::component('public-menu', \Bittacora\PublicMenu\Http\Livewire\PublicMenu::class);
        Livewire::component('public-menu-datatable', MenuDatatable::class);

        $this->publishes([
            __DIR__.'/../resources/js' => public_path('vendor/bittacora/public-menu'),
        ], 'public');

        $this->overrideConfig();

        parent::boot();
    }

    /**
     * Tengo que cargar la configuración de esta forma un poco pirata para sobreescribir la del paquete
     * harimayco/wmenu-builder
     */
    private function overrideConfig(): void
    {
        $config = include __DIR__ . '/../config/public-menu.php';
        config(['menu.table_prefix' => $config['table_prefix']]);
        config(['menu.table_name_menus' => $config['table_name_menus']]);
        config(['menu.table_name_items' => $config['table_name_items']]);
        config(['menu.use_roles' => $config['use_roles']]);
        config(['menu.roles_table' => $config['roles_table']]);
        config(['menu.roles_pk' => $config['roles_pk']]);
        config(['menu.roles_title_field' => $config['roles_title_field']]);
    }
}
