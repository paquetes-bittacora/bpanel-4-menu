<?php

declare(strict_types=1);

namespace NguyenHuy\Menu;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use NguyenHuy\Menu\Models\MenuItems;
use NguyenHuy\Menu\Models\Menus;

/**
 * Class WMenu
 * @package Harimayco\Menu
 */
class WMenu
{
    /**
     * @return Application|Factory|View
     */
    public function render($menuId, $menuModel, $locale)
    {
        $menu = new Menus();
        $menuitems = new MenuItems();
        $menulist = $menu->select(['id', 'name'])->get();
        $menulist = $menulist->pluck('name', 'id')->prepend('Select menu', 0)->all();

        //$roles = Role::all();

        if ((request()->has("action") && empty(request()->input("menu"))) || '0' == request()->input("menu")) {
            return view('wbpanel4-public-menu::menu-html')->with("menulist", $menulist);
        }

        $menu = Menus::whereId($menuModel->id)->firstOrFail();
        $menus = $menuitems->whereParent(0)->whereMenu($menu->id)->get()->all();

        $data = ['menus' => $menus, 'indmenu' => $menu, 'menulist' => $menulist];
        if (config('menu.use_roles')) {
            $data['roles'] = DB::table(config('menu.roles_table'))->select([config('menu.roles_pk'), config('menu.roles_title_field')])->get();
            $data['role_pk'] = config('menu.roles_pk');
            $data['role_title_field'] = config('menu.roles_title_field');
        }

        $data['menuModel'] = $menuModel;
        $data['locale'] = $locale;

        return view('bpanel4-public-menu::overrides.menu-html', $data);
    }

    /**
     * @return Application|Factory|View
     */
    public function scripts()
    {
        return view('bpanel4-public-menu::overrides.scripts');
    }

    /**
     * @param string $name
     * @param array $menulist
     * @return string
     */
    public function select($name = "menu", $menulist = [])
    {
        $html = '<select name="' . $name . '">';

        foreach ($menulist as $key => $val) {
            $active = '';
            if (request()->input('menu') == $key) {
                $active = 'selected="selected"';
            }
            $html .= '<option ' . $active . ' value="' . $key . '">' . $val . '</option>';
        }
        $html .= '</select>';
        return $html;
    }


    /**
     * Returns empty array if menu not found now.
     * Thanks @sovichet
     *
     * @param $name
     * @return array
     */
    public static function getByName($name): array
    {
        $menu = Menus::byName($name);
        return is_null($menu) ? [] : self::get($menu->id);
    }

    /**
     * @param $menuId
     * @return array
     */
    public static function get($menuId)
    {
        $menuItem = new MenuItems();
        $menu_list = $menuItem->getall($menuId);

        $roots = $menu_list->where('menu', $menuId)->where('parent', 0);

        $items = self::tree($roots, $menu_list);
        return $items;
    }

    /**
     * @param $items
     * @param $allItems
     * @return array
     */
    private static function tree($items, $allItems)
    {
        $data_arr = [];
        $i = 0;
        foreach ($items as $item) {
            $data_arr[$i] = $item->toArray();
            $find = $allItems->where('parent', $item->id);

            $data_arr[$i]['child'] = [];

            if ($find->count()) {
                $data_arr[$i]['child'] = self::tree($find, $allItems);
            }

            $i++;
        }

        return $data_arr;
    }
}
