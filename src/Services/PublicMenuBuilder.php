<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Services;

use Bittacora\PublicMenu\Models\MenuModel;
use Illuminate\Support\Facades\Auth;
use NguyenHuy\Menu\Facades\Menu;
use Spatie\Permission\Models\Role;

final class PublicMenuBuilder
{
    public function build(string $menuSlug): array
    {
        $menuModel = MenuModel::whereSlug($menuSlug)->first();

        if (null === $menuModel) {
            return [];
        }

        return $this->filterMenuItems(Menu::get($menuModel->id));
    }

    private function filterMenuItems(array $menu): array
    {
        foreach ($menu as $key => &$item) {
            if (!$this->userCanSeeMenuItem($item)) {
                unset($menu[$key]);
                continue;
            }
            if (!empty($item['child'])) {
                $item['child'] = $this->filterMenuItems($item['child']);
            }
        }

        return $menu;
    }

    private function userCanSeeMenuItem($item): bool
    {
        return null === $item['role_id'] or 0 === (int)$item['role_id'] or (
            null !== Auth::user() and
            Auth::user()->hasRole(Role::whereId($item['role_id'])->first())
        );
    }
}
