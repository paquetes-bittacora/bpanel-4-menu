<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\DataTables;

use Bittacora\PublicMenu\Models\MenuModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class MenuDatatable extends DataTableComponent
{
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Título', 'name')->addClass("w-30"),
            Column::make('Activo', 'active')->addClass("w-30 text-center"),
            Column::make('Idiomas', 'languages')->addClass("w-30 text-center"),
            Column::make('Acciones')->addClass("w-10 text-center"),
        ];
    }

    /**
     * @return Builder<MenuModel>
     */
    public function query(): Builder
    {
        return MenuModel::query()
            ->where('slug', 'LIKE', DB::raw('CONCAT(id, "_%")'))
            ->orderBy('created_at', 'DESC');
    }

    public function rowView(): string
    {
        return 'bpanel4-public-menu::.livewire.menu-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            MenuModel::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }
}
