<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Commands;

use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Comando para instalar el módulo de menús.
 * @package Bittacora\PublicMenu\Commands
 */
class InstallCommand extends Command
{
    public $signature = 'bpanel4-menu:install';

    public $description = 'Instala el módulo de menús';

    public function handle(): void
    {
        $this->comment('Registrando elementos del menú...');
        $module = AdminMenuFacade::createModule('configuration', 'menu', 'Menú público', 'fa fa-list');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');
        $this->comment('Hecho');

        $this->comment('Dando permisos al administrador...');
        $permissions = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update', 'reorder', 'create-menu-item'];
        $adminRole = Role::findOrCreate('admin');
        foreach ($permissions as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'menu.'.$permission]);
            /** @phpstan-ignore-next-line  */
            $adminRole->givePermissionTo($permission);
        }
        $this->comment('Hecho');
    }
}
