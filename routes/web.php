<?php

declare(strict_types=1);

use Bittacora\PublicMenu\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('menu.')->group(function () {
    Route::get('menu', [MenuController::class, 'index'])->name('index');
    Route::get('menu/create', [MenuController::class, 'create'])->name('create');
    Route::post('menu/store', [MenuController::class, 'store'])->name('store');
    Route::get('menu/{model}/edit/language/{locale?}', [MenuController::class, 'editMenu'])->name('edit');
    Route::put('menu/{model}/update/language/{locale?}', [MenuController::class, 'update'])->name('update');
    Route::delete('menu/{model}', [MenuController::class, 'destroy'])->name('destroy');
    Route::get('menu/create-menu-item', [MenuController::class, 'createMenuItem'])->name('create-menu-item');
    Route::post('menu/reorder', [MenuController::class, 'reorder'])->name('reorder');
});
