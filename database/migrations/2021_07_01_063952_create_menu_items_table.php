<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateMenuItemsTable
 */
class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_menu_items', function (Blueprint $table) {
            $table->id();
            $table->string('text')->nullable();
            $table->string('type')->nullable();
            $table->string('route_name')->nullable();
            $table->string('url')->nullable();
            $table->string('icon')->nullable();
            $table->boolean('new_tab')->default(0);
            $table->boolean('active')->default(1);
            $table->string('label');
            $table->string('link');
            $table->integer('depth')->default(0);
            $table->string('class')->nullable();
            $table->string('target')->nullable();
            $table->unsignedBigInteger('menu');
            $table->string('locale')->nullable();
            $table->unsignedBigInteger('sort')->default(0);
            $table->unsignedBigInteger('parent')->default(0);
            $table->timestamps();

            $table->foreign('menu')->references('id')->on('public_menus')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_menu_items');
    }
}
