<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Console\Exception\CommandNotFoundException;

return new class () extends Migration {
    /**
     * @throws CommandNotFoundException
     */
    public function up(): void
    {
        Schema::create('public_menus', function (Blueprint $table) {
            $table->id();
            $table->string('id')->change();
            $table->string('name');
            $table->string('slug');
            $table->string('class')->nullable();
            $table->unsignedInteger('order_column')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop('public_menus');
    }
};
