<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AlterHarimaycoWmenuBuilderTables
 */
class AlterHarimaycoWmenuBuilderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table(config('menu.table_prefix') . config('menu.table_name_items'), function (Blueprint $table) {
            $table->boolean('only_unregistered')->default(0);
        });

        Schema::table(config('menu.table_prefix') . config('menu.table_name_items'), function (Blueprint $table) {
            $table->string('role_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table(config('menu.table_prefix') . config('menu.table_name_items'), function (Blueprint $table) {
            $table->dropColumn('only_unregistered');
        });
    }
}
