<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Database\Seeders;

use Bittacora\PublicMenu\Database\Seeders\Seeds\PublicMenuSeeder;
use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 * @package Bittacora\PublicMenu\Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PublicMenuSeeder::class,
        ]);
    }
}
