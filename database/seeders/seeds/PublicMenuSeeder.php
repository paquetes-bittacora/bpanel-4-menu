<?php

declare(strict_types=1);

namespace Bittacora\PublicMenu\Database\Seeders\Seeds;

use Bittacora\Language\LanguageFacade;
use Bittacora\PublicMenu\Models\MenuModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class PublicMenuSeeder
 * @package Bittacora\PublicMenu\Database\Seeders\Seeds
 */
class PublicMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainMenu = new MenuModel();
        $mainMenu->setLocale('es');
        $mainMenu->name = 'Menú principal';
        $mainMenu->slug = 'menu-principal';
        $mainMenu->active = true;
        $mainMenu->order_column = 1;
        $mainMenu->save();

        $languages = array_column(LanguageFacade::getActives()->toArray(), 'locale');

        foreach ($languages as $language) {
            $menuId = $mainMenu->id . '_' . $language;
            DB::table(config('menu.table_prefix') . config('menu.table_name_menus'))->insert([
                'id' => $menuId,
                'name' => $menuId,
            ]);

//            DB::table(config('menu.table_prefix') . config('menu.table_name_items'))->insert([
//                'menu' => $menuId,
//                'label' => 'Inicio',
//                'link' => '/',
//                'parent' => 0,
//                'sort' => 0,
//                'depth' => 0
//            ]);
        }
    }
}
